<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', 'HomeController@index');
Route::get('/register', 'AuthController@index')->name('register');
Route::post('/welcome', 'AuthController@store')->name('welcome');

Route::get('/', 'DashboardController@index');
Route::get('/table', 'DashboardController@table')->name('table');
Route::get('/data-tables', 'DashboardController@dataTable')->name('dataTable');

Route::resource('casts', 'CastController');

Route::get('/{any}', 'HomeController@index'); // Jika url tidak benar
