@extends('layouts.master')
@section('title', 'Edit Cast')

@section('content')
@extends('layouts.master')
@section('title', 'Create Cast')

@section('content')
<form action="{{ route('casts.update', $data->id) }}" method="POST">
    @method('PUT')
    @csrf
    <input type="hidden" name="id" value="{{ $data->id }}"> <br />

    <div class="form-group">
        <label for="nama">Nama</label>
        <input type="text" class="form-control" value="{{ $data->nama }}" name="nama" placeholder="Masukan nama">
    </div>
    <div class="form-group">
        <label for="umur">Umur</label>
        <input type="number" class="form-control" value="{{ $data->umur }}" name="umur" placeholder="Masukan umur">
    </div>
    <div class="form-group">
        <label for="bio">Bio</label>
        <textarea class="form-control" value="{{ $data->bio }}" name="bio" rows="3"></textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection

@endsection
