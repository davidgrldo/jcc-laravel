@extends('layouts.master')
@section('title', 'Show')
@section('content')
<div class="card">
    <div class="card-header">
        {{ $data->nama }} - {{ $data->umur }} tahun
    </div>
    <div class="card-body">
        <blockquote class="blockquote mb-0">
            <p>{{ $data->bio }}</p>
            <a href="{{ route('casts.index') }}">
                <i class="fas fa-angle-double-left"></i> Back
            </a>
        </blockquote>
    </div>
</div>
@endsection
