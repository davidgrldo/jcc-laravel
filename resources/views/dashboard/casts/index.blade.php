@extends('layouts.master')
@section('title', 'Cast')

@section('button')
<a href="{{ route('casts.create') }}" class="btn-primary btn-sm">Create Cast</a>
@endsection

@section('content')
@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>
        <p>{{ $message }}</p>
    </strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<div class="row">
    @foreach ($data as $cast)
    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 d-flex align-items-stretch flex-column">
        <div class="card bg-light d-flex flex-fill">
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-8">
                                <h2 class="lead"><b>{{ $cast->id }}. {{ $cast->nama }}</b></h2>
                            </div>
                            <div class="col-4">
                                <h6 class="text-muted">{{ $cast->umur }} Tahun</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <p class="text-muted text-sm">
                            <b>Bio: </b> {{ $cast->bio }}
                        </p>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="text-right">
                    <form action="{{ route('casts.destroy', $cast->id) }}" method="POST">
                        <a href="{{ route('casts.show', $cast->id) }}" class="btn btn-sm btn-primary">
                            <i class="fas fa-user"></i> View
                        </a>
                        <a href="{{ route('casts.edit', $cast->id) }}" class="btn btn-sm btn-info">
                            <i class="fas fa-pencil-alt"></i> Edit
                        </a>

                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i>Delete</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>
@endsection
