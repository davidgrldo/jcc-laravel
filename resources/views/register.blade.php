<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

    <!-- Styles -->
    <style>
        html,
        body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            /* margin: 0; */
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links>a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form method="POST" action="{{ route('welcome') }}" id="form-pendaftaran">
        @csrf
        <p>First Name: </p>
        <input type="text" id="fname" name="fname">

        <p>Last Name: </p>
        <input type="text" id="lname" name="lname">

        <p>Gender:</p>
        <input type="radio" id="gender" name="gender" value="Male" checked>
        <label for="male">Male</label><br>

        <input type="radio" id="gender" name="gender" value="Female">
        <label for="female">Female</label><br>

        <input type="radio" id="gender" name="gender" value="Other">
        <label for="other">Other</label><br>

        <p>Nationality: </p>
        <select id="nationality" name="nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Singaporean">Singaporean</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Australian">Australian</option>
        </select>

        <p>Language Spoken: </p>
        <input type="checkbox" id="language1" name="language[]" value="Bahasa Indonesia" checked>
        <label for="language1"> Bahasa Indonesia</label><br>
        <input type="checkbox" id="language2" name="language[]" value="English">
        <label for="language2"> English</label><br>
        <input type="checkbox" id="language3" name="language[]" value="Other">
        <label for="language3"> Other</label>

        <p>Bio: </p>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>

        <input type="submit" form="form-pendaftaran" value="Submit">
    </form>
</body>

</html>
