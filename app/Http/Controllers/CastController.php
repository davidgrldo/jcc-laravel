<?php

namespace App\Http\Controllers;

use App\Models\Cast;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $data = DB::table('casts')->get();
        $data = Cast::get();
        return view('dashboard.casts.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.casts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // DB::table('casts')->insert([
        //     'nama' => $request->nama,
        //     'umur' => $request->umur,
        //     'bio' => $request->bio,
        //     'created_at' => Carbon::now()
        // ]);
        Cast::create($request->except('updated_at'));
        return redirect()->route('casts.index')->with('success', 'Cast data created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $data = DB::table('casts')->where('id', $id)->first();
        $data = Cast::findOrFail($id);
        return view('dashboard.casts.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $data = DB::table('casts')->where('id', $id)->first();
        $data = Cast::findOrFail($id);
        return view('dashboard.casts.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // DB::table('casts')->where('id', $id)->update([
        //     'nama' => $request->nama,
        //     'umur' => $request->umur,
        //     'bio' => $request->bio,
        //     'updated_at' => Carbon::now()
        // ]);
        Cast::findOrFail($id)->update($request->except('created_at'));
        return redirect()->route('casts.index')->with('success', 'Cast data updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // DB::table('casts')->where('id', $id)->delete();
        Cast::findOrFail($id)->delete();
        return redirect()->route('casts.index')->with('success', 'Cast data deleted successfully');
    }
}
