<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index()
    {
        return view('register');
    }

    public function store(Request $request)
    {
        $name = $request->fname . ' ' . $request->lname;
        $data = $request->except('fname', 'lname');
        $data['name'] = $name;
        // dd($data);
        return view('home', compact('data'));
    }
}
